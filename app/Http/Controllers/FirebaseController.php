<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class FirebaseController extends Controller{

    public function index()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/prueba-babde-firebase-adminsdk-feg4o-f30d117153.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://prueba-babde.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();

        $newPost = $database
            ->getReference('Consejos')
            ->push([
                'Titulo' => 'Cambiar',
                'Descripcion' => 'cambiar'
                ]);

        //$newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-
        //$newPost->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-
        //$newPost->getChild('title')->set('Changed post title');
        //$newPost->getValue(); // Fetches the data from the realtime database
        //$newPost->remove();

        return $newPost->getvalue();
    }

    public function show($tip){
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/prueba-babde-firebase-adminsdk-feg4o-f30d117153.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://prueba-babde.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();

        $newPost = $database
            ->getReference('Consejos')
            ->getChild($tip);
            
        return $newPost->getvalue();
    }

    public function store(Request $request)
    {
        //Reglas de 
        $rule = [
            //max: maximo de caracteres
            'Titulo' => 'required',
            //in:que solo acepte cierto valores
            'Descripcion'=> 'required'
        ];
        $this->validate($request, $rule);
        //devuelve un array asociativo
        //$authors = Author::create($request->all());
        $xd = $request->all();
        //echo $xd;

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/prueba-babde-firebase-adminsdk-feg4o-f30d117153.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://prueba-babde.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();

        $newPost = $database
            ->getReference('Consejos')
            ->push([$request->all()
                ]);
        $xd="xd";
        return $xd; //$newPost->getvalue();
        
    }


    public function update(Request $request, User $user)
    {
        echo "funca";
    }

    
    public function destroy(User $user)
    {
        echo "funca";
    }
}

?>